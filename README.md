# Node Cluster App Template

## Introduction
A simple [Node.js](https://nodejs.org/) [cluster](https://nodejs.org/dist/latest-v6.x/docs/api/cluster.html) server running [Express.js](https://expressjs.com/). This code is only used for testing purposes and not production ready.

## Features
  - Configuration parameters in `./config/config.js`
  - Master process will fork `n` worker processes based on the number of CPUs. The `config.process.max_workers` variable can be used to override this setting
  - Master process will keep count of the number of requests served by the worker processes
  - Worker processes will report `worker.id` when a GET request is sent to `/api/worker`
  - `SIGUSR2` signal sent to the master process will print the statistics to the console

## Installation

This app requires [Node.js](https://nodejs.org/) v6+ to run. Install the dependencies and start the server.

```sh
$ cd node-cluster-app
$ npm install
$ npm start
```

By default, this will start a single master process and 2 worker processes if you have at least a dual core CPU.

You will get an output like this.

```sh
> node-cluster-app@1.0.0 start /home/ibrahim/workspace/node-cluster-app
> node express-cluster.js

master process starting...
master process 17841 started. forking worker threads
worker 17847 started
worker 17853 started
```
Take note of the master process `PID` (17841 in the example above).

## Usage

1. Send a GET request to `/api/worker`

    ```sh
    $ curl -s http://localhost:8000/api/worker
    ```

    You should get a JSON output similar to the one below.

    ```json
    {"message":"hello from worker thread #1"}
    ```
2. Send a POST request to `/api/worker`

    ```sh
    $ curl -sX POST http://localhost:8000/api/worker
    ```
    You will not get any response.

3. Take note of the master process `PID` from the console log output and send a `SIGUSR2` signal to get the statistics. Sending the `SIGHUP` signal to the worker processes will not work.

    ```sh
    $ kill -USR2 <master-process-pid>
    ```
    The master process should print the stats to the console. It should look something like this.

    ```json
    process_stats{
      "num_requests": 5,
      "num_get_requests": 2
    }
    ```

## Copyright and License
Copyright (c) 2017, Mohamed Ibrahim. Code released under the [ISC](LICENSE) license.