var config = {};

config.app = {}
config.server = {};
config.process = {};

// set app environment
config.app.env = process.env.NODE_ENV || 'development';

// port to listen on
config.server.port = process.env.PORT || 8000;

// interface to listen
config.server.hostname = "0.0.0.0";

// maximum number of worker threads to fork
config.process.max_workers = 2;


module.exports = config;
