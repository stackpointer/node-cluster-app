const config = require('../config/config');
const os = require('os');
const cluster = require('cluster');

var MasterProcess = function () {
    // stats counters
    this.process_stats = {
        num_requests : 0,       // total number of requests to /api
        num_get_requests : 0    // total number of GET requests to /api
    };
    console.log("master process starting...");
}

// initialise and fork worker processes
MasterProcess.prototype.init = function init() {
    // NOTE: this is not an anonymous function as we will not be able to access
    // this.process_stats variable inside an anonymous function

    console.log(`master process ${process.pid} started. forking worker threads`);

    // NOTE: due to hyperthreading, OS might report more cpu cores
    let cpu_count = os.cpus().length;

    // determine number of workers to fork
    // use the smaller value of max_workers and cpu count
    let num_workers = (config.process.max_workers >= 1 && config.process.max_workers <= cpu_count) ?
            config.process.max_workers : cpu_count;

    for (let i=0; i<num_workers; i++) {
        cluster.fork();
    }

    cluster.on('online', (worker) => {
        console.log(`worker ${worker.process.pid} started`);
    });

    // restart worker process if it is stopped
    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} killed using code: ${code}, signal: ${signal}`);
        cluster.fork();
    });

    // use bind(this) to pass this parameter to callback
    // refer to: https://stackoverflow.com/questions/20279484/how-to-access-the-correct-this-context-inside-a-callback
    cluster.on('message', (function message_handler (worker, msg, handle) {
        if(msg.command=="update_counter" && this.process_stats.hasOwnProperty(msg.counter)) {
            this.process_stats[msg.counter]++;
        }
    }).bind(this));

    // display stats when we receive SIGUSR2 signal
    process.on('SIGUSR2',(function display_stats() {
        console.log("process_stats" + JSON.stringify(this.process_stats, undefined, 2));
    }).bind(this));
}

module.exports = new MasterProcess();
