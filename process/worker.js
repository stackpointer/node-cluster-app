const config = require('../config/config');
const os = require('os');
const cluster = require('cluster');
const express = require('express');
const app = express();

function WorkerProcess() {

}

// start worker process
WorkerProcess.prototype.start = () => {
    // set port
    var port = config.server.port;

    // set router
    var router = express.Router();


    // intercept all routes to router object
    router.use(function(req, res, next) {
        cluster.worker.send({ command: "update_counter", counter: 'num_requests' });
        next();
    });

    // send hello identifying the worker thread
    router.get('/worker', function(req, res) {
        cluster.worker.send({ command: "update_counter", counter: 'num_get_requests' });
        console.log(`worker #${cluster.worker.id} processing request`)
        res.json({ message: `hello from worker thread #${cluster.worker.id}` });
    });

    // mount router routes under /api
    app.use('/api', router);

    app.listen(port);

}

module.exports = new WorkerProcess();
