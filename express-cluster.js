const config = require('./config/config');
const cluster = require('cluster');

if (cluster.isMaster) {
    // if masterProcess is created before this, each worker process will also create a new masterProcess()
    const masterProcess = require('./process/master');
    masterProcess.init();
} else {
    const workerProcess = require('./process/worker');
    workerProcess.start();
}